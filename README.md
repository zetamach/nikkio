# Nikkio Social Audio #

## README ##
This is a set of scripts to easily share song links and titles.

fetter (g) 2015

**Installation**

Put the scripts wherever you want.
I'll make an installer to a standard location eventually.
This is just a quick package.

**Setup**

Setup your paths in all the scripts:

set_now_playing.sh

get_now_playing.sh


**IRC** 

See 1_HOWTO_irc_alias for how to setup an irc alias

**MPSYT**

Look in the mpsyt_mods folder to see how to modify mpsyt.


You will also need to run mpsyt and mod the config with the set command to set the notifier to set_now_playing.sh:

$ mpsyt

set notifier /path/to/set_now_playing.sh



Done. If you setup the irc alias as described in the howto file,
from irc, you can do /np to show what you're listening to at that moment.

### Who do I talk to? ###

* Repo owner or admin