#!/bin/bash
# this is the app that gets notified w current song title of what is playing in mpsyt
#

# fetter 2015

# filter all unsafe staff before echo so title chars cant fux w bash
printf -v var "%q\n" "$*"
echo "$var"

# reverse escaping as going to file
echo $var | sed 's/\\\(.\)/\1/g' > /home/c/swd/now_playing/now_playing_in_mpsyt.txt

# The following line tweets the current song
# just comment out if you dont have it or dont want to use it
/home/c/swd/now_playing/set_zs_now_playing.sh

# The following line sends the current song to facebook
#/home/c/swd/now_playing/set_fb_zs_now_playing.sh
